package MaximumSubArray;

public class Main {

    public static void main(String[] args) {

        //The solution for the first assignment.
        //Every String ends in a designated position.

        Padder p = new Padder(25, ' ');

        System.out.println("1.assignment");
        System.out.println(p.pad("12345678901234567"));
        System.out.println(p.pad("Joe"));
        System.out.println(p.pad("George"));
        System.out.println((p.pad("Maximillian")));

        //The solution for the second assignment.
        //Every Integer Array in a designated position.

        System.out.println("--------------------------------");

        System.out.println("2.assignment");

        int[] arr = {-14, -1, 2, 3, -9, 11};

        System.out.println(p.printEin(arr, 5, 2));

        //The solution for the third assignment.
        //Designated position and sum of a specific subarray.

        System.out.println("--------------------------------");

        System.out.println("3. assignment");

        TheManipulatorOfSubArrays m = new TheManipulatorOfSubArrays();

        System.out.println(p.printEin(arr, 5, 6)+ " => " + m.EinSum(arr, 5, 6));

        //The solution for the fourth assignment.
        //Maximum sum of subarrays brute force.
        //+Fourth assignment extratask: write out the subarray with the maximum sum.

        System.out.println("--------------------------------");

        System.out.println("4. assignment");

        m.MaxSubArrayBrute(arr);

        //The solution for the fifth assignment.
        //Kadane's algorhythm.

        System.out.println("--------------------------------");

        System.out.println("5. assignment");

        System.out.println(m.maxSumKadane(arr));

    }

}
